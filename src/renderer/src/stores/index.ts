import { configureStore } from '@reduxjs/toolkit';

import demoReducer from './demoStore/index';

export const store = configureStore({
  reducer: {
    demoReducer: demoReducer,
  },
});
