import { createSlice, PayloadAction } from '@reduxjs/toolkit';

type data = {
  id: number;
  name: string;
  type: string;
};

const initialState: data[] = [];

export const demoStore = createSlice({
  name: 'demoStore',
  initialState,
  reducers: {
    setData: (state, action: PayloadAction<data[]>) => {
      state = action.payload;
      console.log('🚀 ~ file: index.ts:17 ~ state:', state);
    },
  },
});

export const { setData } = demoStore.actions;

export const getListData = (state: unknown): data => state as data;

export default demoStore.reducer;
