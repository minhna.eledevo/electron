import React from 'react';
import { Avatar, Card, message } from 'antd';

const { Meta } = Card;

const UserInfo: React.FC = () => {
  const handleClickUser = (): void => {
    message.success('User info');
  };
  return (
    <Card className="userInfo" onClick={handleClickUser}>
      <Meta
        avatar={<Avatar size="large" src="{uri}" alt="Đây là ảnh đại diện" />}
        title="User Name"
        description="Role"
      />
    </Card>
  );
};
export default UserInfo;
