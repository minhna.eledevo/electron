import React from 'react';
import { Outlet } from 'react-router-dom';
import { Layout } from 'antd';

import SiderMenu from '../SiderMenu';
import UserInfo from './userInfo';

import './index.scss';

const { Header, Sider, Content } = Layout;

const LayoutDefault: React.FC = () => (
  <Layout className="layout_container">
    <Sider className="layout_container-sider">
      <UserInfo />
      <SiderMenu />
    </Sider>
    <Layout>
      <Header className="layout_container-header"></Header>
      <Content className="layout-content">
        <Outlet />
      </Content>
    </Layout>
  </Layout>
);
export default LayoutDefault;
