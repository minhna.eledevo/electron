import BasicTable from './src/BasicTable';
import { useTableActionsBuilder } from './src/tableActionsBuilder';

export { BasicTable, useTableActionsBuilder };
