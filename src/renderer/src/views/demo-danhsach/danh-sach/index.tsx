import React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { BasePagination } from '@/apis/typesBase';
import { ActionItem } from '@/components/Table/src/types/tableAction';

import { columns } from './table.data.config';
import { IDataDemo } from './types';
import { useDemoApi } from '@/apis';
import { getListData, setData } from '@/stores/demoStore';
import { BasicTable, useTableActionsBuilder } from '@/components/Table';

const Demo: React.FC = () => {
  const { demoApi } = useDemoApi();
  const dispatch = useDispatch();
  const dataStore = useSelector(getListData);
  const actionColum = {
    actions: (record: IDataDemo): ActionItem[] => {
      const [builder] = useTableActionsBuilder();
      const action = builder
        // .addView({
        //   onClick: () => goDetailStatus(record.id),
        // })
        // .addEdit({ onClick: () => goUpdateStatus(record.id) })
        // .addRemove(() => handleDelete(record.id))
        .build();
      return action;
    },
  };
  const handleApi = async (params: { name: string }): Promise<BasePagination<any>> => {
    let dataForm: any = null;
    if (!params.name) {
      const transParams = {
        name: params.name,
      };
      dataForm = await demoApi.getAll(transParams);
    } else {
      dataForm = await demoApi.getAll();
    }
    console.log('🚀 ~ file: index.tsx:35 ~ handleApi ~ dataForm:', dataForm);
    dispatch(setData(dataForm));
    return {
      items: dataForm,
    };
  };
  return <BasicTable columns={columns} actionColum={actionColum} api={handleApi} />;
};

export default Demo;
