export interface IDataDemo {
  id: number;
  name: string;
  type: string;
}
