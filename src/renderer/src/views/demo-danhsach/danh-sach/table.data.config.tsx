import { BasicColumnModel } from '/@/components/Table/src/types/table';

import { IDataDemo } from './types';

export const columns: BasicColumnModel<IDataDemo>[] = [
  {
    title: 'Name',
    dataIndex: 'name',
    key: 'name',
  },
  {
    title: 'Type',
    dataIndex: 'type',
    key: 'type',
  },
];
