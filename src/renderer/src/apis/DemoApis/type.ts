import { BaseInfo } from '../typesBase';

export interface IDemo extends BaseInfo {
  name: string;
  type: string;
}
