import { BaseApi } from '../baseApi';
import { IDemo } from './type';
import { DOMAIN_DEVELOPMENT, EndPoint } from '../apiConfig';

export class DemoApi extends BaseApi<IDemo> {
  constructor() {
    super(`${DOMAIN_DEVELOPMENT}/${EndPoint.demo}`);
  }
}
