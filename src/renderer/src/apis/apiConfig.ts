// Create static variables related to api
// export const DOMAIN_DEVELOPMENT = "http://localhost:3001"
export const DOMAIN_DEVELOPMENT = 'http://192.168.1.5:8081';

export enum HTTP_METHOD {
  GET = 'GET',
  POST = 'POST',
  PUT = 'PUT',
  DELETE = 'DELETE',
}

export const HTTP_HEADER = {
  JSON: { 'Content-Type': 'application/json' },
};

export enum ContentTypeEnum {
  // json
  JSON = 'application/json;charset=UTF-8',
}

export enum EndPoint {
  demo = 'folder/get/1',
}
