import Axios from 'axios';
import { AxiosRequestConfig } from 'axios';

import { BaseInfo, BasePagination, IBaseSearchListQueryParams } from './typesBase';
import { ContentTypeEnum, HTTP_METHOD } from './apiConfig';

const token = localStorage.getItem('token');

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
async function apiCommon<T>(params: AxiosRequestConfig) {
  return Axios<T>({
    headers: { token: token, 'Content-type': ContentTypeEnum.JSON },
    ...params,
  })
    .then((res) => res.data)
    .catch((res) => res.error);
}
export abstract class BaseApi<T extends BaseInfo> {
  constructor(protected configUrl: string) {}

  async getAll<R, P>(queries = {} as IBaseSearchListQueryParams & R): Promise<BasePagination<P>> {
    return apiCommon<BasePagination<P>>({
      url: this.configUrl,
      method: HTTP_METHOD.GET,
      params: queries,
    });
  }

  async getById<T>(id: string): Promise<T> {
    const url = `${this.configUrl}/${id}`;
    return apiCommon<T>({ url: url, method: HTTP_METHOD.GET });
  }

  async updateById<T, R>(id: string, body: R): Promise<T> {
    const url = `${this.configUrl}/${id}`;
    return apiCommon<T>({ url: url, method: HTTP_METHOD.PUT, data: body });
  }

  async create<T, R>(body: R): Promise<T> {
    return apiCommon<T>({ url: this.configUrl, method: HTTP_METHOD.POST, data: body });
  }

  async deleteById<T>(id: string): Promise<T> {
    const url = `${this.configUrl}/${id}`;
    return apiCommon<T>({ url: url, method: HTTP_METHOD.DELETE });
  }
}
