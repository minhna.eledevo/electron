import { XuLyDemoApi } from './demoApis';

let apiDemoInstance: InstanceType<typeof XuLyDemoApi>;

export const useDemoApi = (): XuLyDemoApi => {
  if (!apiDemoInstance) {
    apiDemoInstance = new XuLyDemoApi();
  }
  return apiDemoInstance;
};
