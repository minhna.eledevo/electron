export interface IBaseSearchListQueryParams {
  timKiemNhanh?: string;
  page?: number;
  pageSize?: number;
  isFetchAll?: boolean;
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export interface BasePagination<T = any> {
  items: T[];
  // total: number;
}

export enum Domain {
  ITEM = '/ITEM',
}

export interface BaseInfo {
  statusResponse: boolean;
  messageResponse: string;
}
