import React from 'react';
import ReactDOM from 'react-dom/client';
import { Provider } from 'react-redux';

import AppRoutes from './router/index';
import { store } from './stores/index';
import './locales/i18n';
import './assets/index.css';

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <Provider store={store}>
    <AppRoutes />
  </Provider>
);
