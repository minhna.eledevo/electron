import { Recordable } from '@renderer/utils';
import { genMessage } from '../helper';

const modules = import.meta.glob('./vi-VN/**/*.ts', { eager: true });
export default {
  ...genMessage(modules as Recordable<Recordable>, 'vi-VN'),
};
