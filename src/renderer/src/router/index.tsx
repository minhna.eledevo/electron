import { BrowserRouter, Route, Routes } from 'react-router-dom';

import LayoutDefault from '../components/layout';
import { transformRoute } from './transformRoute';

const AppRoutes: React.FC = () => (
  <BrowserRouter>
    <Routes>
      <Route path="/" element={<LayoutDefault />}>
        {transformRoute()}
      </Route>
    </Routes>
  </BrowserRouter>
);
export default AppRoutes;
