import { RouteType } from './config';
import { DemoDanhSach } from '../views';

const appRoutes: RouteType[] = [
  {
    path: '/demo-danh-sach',
    element: <DemoDanhSach />,
    state: 'demo-danh-sach',
    sidebarProps: {
      displayText: 'Demo Danh Sach',
    },
  },
  // {
  //   path: '/quan-ly-tai-khoan',
  //   element: <></>,
  //   state: 'Dashboard',
  // },
  // {
  //   path: '/quan-ly-don-vi',
  //   element: <></>,
  //   state: 'Dashboard',
  // },
  // {
  //   path: '/quan-ly-so-do-mau',
  //   element: <></>,
  //   state: 'Dashboard',
  // },
  // {
  //   path: '/quan-ly-vu-an',
  //   element: <></>,
  //   state: 'Dashboard',
  // },
  // {
  //   path: '/quan-ly-loai-an',
  //   element: <></>,
  //   state: 'Dashboard',
  // },
  // {
  //   path: '/quan-ly-trang-thai',
  //   element: <></>,
  //   state: 'Dashboard',
  // },
];

export default appRoutes;
